use std::env;

use qchess::*;

fn perft(board: Board, depth: usize) -> Result<usize, Error> {
    if depth == 0 {
        return Ok(1);
    }

    let mut mf = MoveFold::default();
    mf.collect_pseudolegal(&board)?;

    let mut nodes = 0;

    for mv in mf.moves {
        let new_board = board.apply_move(mv)?;
        nodes += perft(new_board, depth - 1)?;
    }

    Ok(nodes)
}

fn perft_top(board: Board, depth: usize) -> Result<(), Error> {
    assert!(depth >= 1, "depth must be 1 or more");

    let mut mf = MoveFold::default();
    mf.collect_pseudolegal(&board)?;

    let mut total_nodes = 0;

    for mv in mf.moves {
        let new_board = board.apply_move(mv)?;
        let new_nodes = perft(new_board, depth - 1)?;

        let (from_file, from_rank) = mv.from.to_human_readable();
        let (to_file, to_rank) = mv.to.to_human_readable();
        println!("{from_file}{from_rank}{to_file}{to_rank} {new_nodes}");

        total_nodes += new_nodes;
    }

    println!("\n{total_nodes}");

    Ok(())
}

pub fn main() -> Result<(), Error> {
    let mut args = env::args().skip(1);
    let depth = args.next().unwrap();
    let fen = args.next().unwrap();
    let moves = args.next();

    if moves.is_some() {
        panic!("moves argument is unsupported");
    }

    let board = Board::from_fen(&fen).unwrap();
    perft_top(board, depth.parse().unwrap())?;

    Ok(())
}

