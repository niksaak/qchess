#![allow(dead_code)]
#![allow(unused_variables)]

use std::collections::{HashSet, HashMap};
use bitvec::prelude::*;

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
#[repr(u8)]
pub enum Color {
    White = 0,
    Black = 1,
}

impl Color {
    pub fn opposite(self) -> Self {
        use Color::*;
        match self {
            White => Black,
            Black => White,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
#[repr(u8)]
pub enum PieceKind {
    Pawn = 0,
    Rook = 1,
    Knight = 2,
    Bishop = 3,
    Queen = 4,
    King = 5,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
#[repr(u8)]
pub enum SpecialMove {
    CastlesKingside,
    CastlesQueenside,
    DoubleAdvance,
    EnPassant,
    Promotion(PieceKind),
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub struct Piece(pub Color, pub PieceKind);

impl Piece {
    pub fn to_bits(self) -> u8 {
        let Piece(color, kind) = self;
        let color_shift = match color {
            Color::White => 1,
            Color::Black => 7,
        };
        kind as u8 + color_shift
    }

    pub fn from_bits(bits: u8) -> Result<Option<Piece>, Error> {
        use Color::*;
        use PieceKind::*;

        let piece = match bits {
            0 => return Ok(None),

            1  => Piece(White, Pawn),
            2  => Piece(White, Rook),
            3  => Piece(White, Knight),
            4  => Piece(White, Bishop),
            5  => Piece(White, Queen),
            6  => Piece(White, King),

            7  => Piece(Black, Pawn),
            8  => Piece(Black, Rook),
            9  => Piece(Black, Knight),
            10 => Piece(Black, Bishop),
            11 => Piece(Black, Queen),
            12 => Piece(Black, King),

            _ => return Err(Error::InvalidPieceBits),
        };
        Ok(Some(piece))
    }

    pub fn from_fen_char(c: char) -> Self {
        use {PieceKind::*, Color::*};
        match c {
            'P' => Piece(White, Pawn),
            'p' => Piece(Black, Pawn),
            'N' => Piece(White, Knight),
            'n' => Piece(Black, Knight),
            'B' => Piece(White, Bishop),
            'b' => Piece(Black, Bishop),
            'R' => Piece(White, Rook),
            'r' => Piece(Black, Rook),
            'Q' => Piece(White, Queen),
            'q' => Piece(Black, Queen),
            'K' => Piece(White, King),
            'k' => Piece(Black, King),
            _ => unreachable!(),
        }
    }

    pub fn to_char(self) -> char {
        use Color::*;
        use PieceKind::*;

        let Piece(color, piece) = self;

        let piece_char = match color {
            White => '♔' as u32,
            Black => '♚' as u32,
        };

        let offset: u32 = match piece {
            King => 0,
            Queen => 1,
            Rook => 2,
            Bishop => 3,
            Knight => 4,
            Pawn => 5,
        };

        char::from_u32(piece_char + offset).unwrap()
    }

    pub fn color(self) -> Color {
        self.0
    }

    pub fn kind(self) -> PieceKind {
        self.1
    }
}

pub type Coord = [i8; 2];

pub fn coord(x: char, y: u8) -> Result<Coord, Error> {
    let x = match x {
        'a' | 'A' => 0,
        'b' | 'B' => 1,
        'c' | 'C' => 2,
        'd' | 'D' => 3,
        'e' | 'E' => 4,
        'f' | 'F' => 5,
        'g' | 'G' => 6,
        'h' | 'H' => 7,
        _ => return Err(Error::InvalidCoord),
    };
    let y = y.checked_sub(1).filter(|&y| y <= 7).ok_or(Error::InvalidCoord)?;
    Ok([x, y as i8])
}

pub trait CoordExt {
    fn is_in_bounds(self) -> bool;
    fn to_index(self) -> usize;
    fn to_human_readable(self) -> (char, u8);
}

impl CoordExt for Coord {
    fn is_in_bounds(self) -> bool {
        self.iter().all(|&n| n >= 0 && n < 8)
    }

    fn to_index(self) -> usize {
        let [x, y] = self;
        (8*y + x) as usize
    }

    fn to_human_readable(self) -> (char, u8) {
        let [x, y] = self;
        let base = 'a' as u32;
        let base_char = char::from_u32(base + x as u32).unwrap();
        (base_char, y as u8 + 1)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub struct Move {
    pub color: Color,
    pub from: Coord,
    pub to: Coord,
    pub special: Option<SpecialMove>,
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct Board {
    squares: BitArray<[u64; 4], Lsb0>,
    to_move: Color,
    castles: BitArray<[u8; 1], Lsb0>,
    en_passant: Option<Coord>,
}

#[derive(Default)]
pub struct MoveFold {
    /// A set of all possible moves for a specific board.
    pub moves: HashSet<Move>,

    /// Reachability bitmaps for white and black.
    pub reachable: [BitArray<[u64; 1], Lsb0>; 2],

    // Keys are pinned pieces, values are attackers.
    pub pins: HashMap<Coord, Coord>,
}

/// Core getters
impl Board {
    pub fn get(&self, coord: Coord) -> Result<Option<Piece>, Error> {
        let index = coord.to_index() * 4;

        let piece_bits = self.squares[index .. (index+4)].load();

        Piece::from_bits(piece_bits)
    }

    pub fn set(&mut self, coord: Coord, piece: Option<Piece>) {
        let index = coord.to_index() * 4;

        match piece {
            Some(piece) => {
                let piece_bits = piece.to_bits();
                self.squares[index .. (index+4)].store(piece_bits);
            }
            None => {
                self.squares[index .. (index+4)].store(0);
            }
        }
    }

    // Returns if queenside and kingside castles are possible for this color.
    pub fn can_castle(&self, color: Color) -> (bool, bool) {
        let offset = color as usize * 2;
        let queenside = self.castles[offset];
        let kingside = self.castles[offset + 1];
        (queenside, kingside)
    }

    pub fn set_castles_queenside(&mut self, color: Color, set: bool) {
        let offset = color as usize * 2;
        self.castles.set(offset, set);
    }

    pub fn set_castles_kingside(&mut self, color: Color, set: bool) {
        let offset = color as usize * 2;
        self.castles.set(offset + 1, set);
    }

    pub fn clear_castles(&mut self, color: Color) {
        let offset = color as usize * 2;
        self.castles[offset..=(offset+1)].store(0);
    }
}

/// Applying moves
impl Board {
    pub fn apply_move(mut self, mv: Move) -> Result<Self, Error> {
        let Some(mut piece) = self.get(mv.from)? else { return Err(Error::NoPieceAtCoordinates) };

        self.set(mv.from, None);

        match mv.special {
            Some(SpecialMove::CastlesKingside) => {
                if piece.kind() != PieceKind::King {
                    return Err(Error::InvalidMove);
                }
                self.set([7, mv.from[1]], None);
                self.set([5, mv.from[1]], Some(Piece(piece.0, PieceKind::Rook)));
                self.clear_castles(piece.color());
            }
            Some(SpecialMove::CastlesQueenside) => {
                if piece.kind() != PieceKind::King {
                    return Err(Error::InvalidMove);
                }
                self.set([0, mv.from[1]], None);
                self.set([3, mv.from[1]], Some(Piece(piece.0, PieceKind::Rook)));
                self.clear_castles(piece.color());
            }
            Some(SpecialMove::DoubleAdvance) => {
                if piece.kind() != PieceKind::Pawn {
                    return Err(Error::InvalidMove);
                }
                self.en_passant = Some(match piece.0 {
                    Color::White => [mv.from[0], 2],
                    Color::Black => [mv.from[0], 5],
                });
            }
            Some(SpecialMove::EnPassant) => {
                if piece.kind() != PieceKind::Pawn {
                    return Err(Error::InvalidMove);
                }
                if self.en_passant != Some(mv.to) {
                    return Err(Error::InvalidMove);
                }
                let Some(target) = self.en_passant else { return Err(Error::NoEnPassantTarget) };
                self.set(target, None);
            }
            Some(SpecialMove::Promotion(kind)) => {
                piece = Piece(piece.0, kind);
            }
            None => (),
        }

        match piece.kind() {
            PieceKind::Rook => {
                if mv.from[0] == 0 {
                    self.set_castles_kingside(piece.color(), false);
                } else if mv.from[0] == 7 {
                    self.set_castles_queenside(piece.color(), false);
                }
            }
            PieceKind::King => {
                self.clear_castles(piece.color());
            }
            _ => (),
        }

        if mv.special != Some(SpecialMove::DoubleAdvance) {
            self.en_passant = None;
        }

        self.set(mv.to, Some(piece));

        self.to_move = match self.to_move {
            Color::White => Color::Black,
            Color::Black => Color::White,
        };

        Ok(self)
    }
}

impl MoveFold {
    pub fn insert(&mut self, mv: Move) {
        self.moves.insert(mv);
        let reachable_index = mv.to.to_index();
        self.reachable[mv.color as usize].set(reachable_index, true);
    }

    pub fn is_reachable(&self, color: Color, coord: Coord) -> bool {
        let index = coord.to_index();
        self.reachable[color as usize][index]
    }
}

/// Move gathering.
impl MoveFold {
    pub fn collect_pseudolegal(&mut self, board: &Board) -> Result<(), Error> {
        for x in 0..8 {
            for y in 0..8 {
                let Some(piece) = board.get([x, y])? else { continue };
                if piece.color() == board.to_move {
                    self.collect_pseudolegal_for(piece, [x, y], board)?;
                }
            }
        }

        self.collect_castles(board)?;

        Ok(())
    }

    pub fn collect_pseudolegal_for(
        &mut self,
        piece: Piece,
        from: Coord,
        board: &Board,
    ) -> Result<(), Error> {
        use PieceKind::*;

        let Piece(color, kind) = piece;

        match kind {
            Pawn => self.collect_moves_for_pawn(color, from, board)?,
            Rook => {
                self.collect_line_moves(color, from,  1,  0, board)?;
                self.collect_line_moves(color, from,  0, -1, board)?;
                self.collect_line_moves(color, from, -1,  0, board)?;
                self.collect_line_moves(color, from,  0,  1, board)?;
            }
            Knight => self.collect_moves_for_knight(color, from, board)?,
            Bishop => {
                self.collect_line_moves(color, from,  1, -1, board)?;
                self.collect_line_moves(color, from, -1, -1, board)?;
                self.collect_line_moves(color, from, -1,  1, board)?;
                self.collect_line_moves(color, from,  1,  1, board)?;
            }
            Queen => {
                self.collect_line_moves(color, from,  1,  0, board)?;
                self.collect_line_moves(color, from,  1, -1, board)?;
                self.collect_line_moves(color, from,  0, -1, board)?;
                self.collect_line_moves(color, from, -1, -1, board)?;
                self.collect_line_moves(color, from, -1,  0, board)?;
                self.collect_line_moves(color, from, -1,  1, board)?;
                self.collect_line_moves(color, from,  0,  1, board)?;
                self.collect_line_moves(color, from,  1,  1, board)?;
            }
            King => self.collect_moves_for_king(color, from, board)?,
        }

        Ok(())
    }

    fn collect_moves_for_pawn(
        &mut self,
        color: Color,
        from: Coord,
        board: &Board,
    ) -> Result<(), Error> {
        use Color::*;
        use SpecialMove::*;

        let direction: i8 = match color {
            White => 1,
            Black => -1,
        };
        let [x, y] = from;

        // Takes
        let take_coords = [
            [x - 1, y + direction],
            [x + 1, y + direction],
        ];
        for to in take_coords {
            if !to.is_in_bounds() {
                continue;
            }

            if let Some(Piece(other_color, _)) = board.get(to)? {
                if other_color != color {
                    let mv = Move { color, from, to, special: None };
                    let is_promotable = (color == White && y + direction == 7)
                        || (color == Black && y + direction == 0);

                    if is_promotable {
                        self.collect_pawn_promotions(mv, board)?;
                    } else {
                        self.insert(mv);
                    }
                }
            } else if let Some(en_passant_target) = board.en_passant {
                if to == en_passant_target {
                    let mv = Move{ color, from, to, special: Some(EnPassant) };
                    self.insert(mv);
                }
            }
        }

        // Advance
        let to = [x, y + direction];
        if !to.is_in_bounds() {
            return Ok(())
        }

        if board.get(to)?.is_none() {
            let mv = Move { color, from, to, special: None };
            let is_promotable = (color == White && y + direction == 7)
                || (color == Black && y + direction == 0);
            if is_promotable {
                self.collect_pawn_promotions(mv, board)?;
            } else {
                self.insert(mv);
            }

            // Double advance
            let to = [x, y + 2*direction];
            let can_double_advance = (color == White && y == 1)
                || (color == Black && y == 6);

            if can_double_advance && board.get(to)?.is_none() {
                let mv = Move{ color, from, to, special: Some(DoubleAdvance) };
                self.insert(mv);
            }
        }

        Ok(())
    }

    fn collect_pawn_promotions(&mut self, mv: Move, board: &Board) -> Result<(), Error> {
        use PieceKind::*;

        let kinds = [Rook, Knight, Bishop, Queen];
        for kind in kinds {
            self.insert(Move{ special: Some(SpecialMove::Promotion(kind)), ..mv });
        }

        Ok(())
    }

    fn collect_moves_for_knight(
        &mut self,
        color: Color,
        from: Coord,
        board: &Board,
    ) -> Result<(), Error> {
        let [x, y] = from;
        let moves = [
            [x + 2, y - 1],
            [x + 1, y - 2],
            [x - 1, y - 2],
            [x - 2, y - 1],
            [x - 2, y + 1],
            [x - 1, y + 2],
            [x + 1, y + 2],
            [x + 2, y + 1],
        ];

        for to in moves {
            if !to.is_in_bounds() {
                continue;
            }
            if matches!(board.get(to)?, Some(Piece(other_color, _)) if other_color == color) {
                continue;
            }
            self.insert(Move{ color, from, to, special: None });
        }

        Ok(())
    }

    fn collect_moves_for_king(&mut self, color: Color, from: Coord, board: &Board) -> Result<(), Error> {
        let [x, y] = from;
        let moves = [
            [x + 1, y + 0],
            [x + 1, y - 1],
            [x + 0, y - 1],
            [x - 1, y - 1],
            [x - 1, y + 0],
            [x - 1, y + 1],
            [x + 0, y + 1],
            [x + 1, y + 1],
        ];

        for to in moves {
            if !to.is_in_bounds() {
                continue;
            }
            if matches!(board.get(to)?, Some(Piece(other_color, _)) if other_color == color) {
                continue;
            }
            self.insert(Move{ color, from, to, special: None });
        }

        Ok(())
    }

    fn collect_castles(&mut self, board: &Board) -> Result<(), Error> {
        use PieceKind::*;
        use Color::*;
        use SpecialMove::*;

        for (color, y) in [(White, 0), (Black, 7)] {
            let has_king = matches!(board.get([4, y])?, Some(Piece(color, King)));

            let (is_queenside_allowed, is_kingside_allowed) = board.can_castle(color);

            let has_queenside_rook = matches!(board.get([0, y])?, Some(Piece(color, Rook)));
            let is_queenside_open =
                matches!(board.get([1, y])?, None)
                && matches!(board.get([2, y])?, None)
                && matches!(board.get([3, y])?, None);
            let can_castle_queenside =
                has_king
                && is_queenside_allowed
                && is_queenside_open
                && has_queenside_rook;

            let has_kingside_rook = matches!(board.get([7, y])?, Some(Piece(color, Rook)));
            let is_kingside_open =
                matches!(board.get([5, y])?, None)
                && matches!(board.get([6, y])?, None);
            let can_castle_kingside =
                has_king
                && is_kingside_allowed
                && is_kingside_open
                && has_kingside_rook;

            if can_castle_queenside {
                self.insert(Move {
                    color,
                    from: [4, y],
                    to: [2, y],
                    special: Some(CastlesQueenside),
                })
            }

            if can_castle_kingside {
                self.insert(Move {
                    color,
                    from: [4, y],
                    to: [6, y],
                    special: Some(CastlesKingside),
                })
            }
        }

        Ok(())
    }

    fn collect_line_moves(
        &mut self,
        color: Color,
        from: Coord,
        x_offset: i8,
        y_offset: i8,
        board: &Board,
    ) -> Result<(), Error> {
        let [mut x, mut y] = from;

        loop {
            x += x_offset;
            y += y_offset;

            if ![x, y].is_in_bounds() {
                break;
            }

            if let Some(Piece(other_color, _)) = board.get([x, y])? {
                if other_color != color {
                    self.insert(Move{ color, from, to: [x, y], special: None });
                }
                break;
            }

            self.insert(Move{ color, from, to: [x, y], special: None });
        }

        Ok(())
    }
}

/// FEN parsing.
impl Board {
    pub fn from_fen(input: &str) -> Result<Self, nom::error::Error<&str>> {
        use std::iter;
        use nom::{
            IResult,
            Finish,
            Parser,
            bytes::complete::tag,
            character::complete::*,
            multi::*,
            branch::*,
            sequence::*
        };

        fn parse_rank(input: &str) -> IResult<&str, Vec<Option<Piece>>> {
            fold_many_m_n(
                1,
                8,
                alt((
                    one_of("PpNnBbRrQqKk"),
                    satisfy(|c| '1' <= c && c <= '8'),
                )),
                Vec::new,
                |mut acc: Vec<Option<Piece>>, c| {
                    let skip = c.to_digit(10);
                    match skip {
                        Some(count) => acc.extend(iter::repeat(None).take(count as usize)),
                        None => acc.push(Some(Piece::from_fen_char(c))),
                    }
                    acc
                },
            )(input)
        }

        fn parse_board(i: &str) -> IResult<&str, Board> {
            // Parse
            let (i, pieces) = separated_list1(tag("/"), parse_rank)(i)?;
            let (i, _) = space1(i)?;

            let (i, to_move) = one_of("wb")(i)?;
            let (i, _) = space1(i)?;

            let (i, castling) = alt((
                tag("-").map(|_| vec![]),
                many_m_n(1, 4, one_of("KQkq")),
            ))(i)?;
            let (i, _) = space1(i)?;

            let (i, en_passant) = alt((
                    tag("-").map(|_| None),
                    tuple((one_of("abcdefgh"), one_of("36"))).map(|t| Some(t))
            ))(i)?;
            let (i, _) = space1(i)?;

            let (i, _halfmove) = u32(i)?;
            let (i, _) = space1(i)?;

            let (i, _fullmove) = u32(i)?;

            // Setup
            let mut board = Board {
                squares: Default::default(),
                to_move: Color::White,
                castles: Default::default(),
                en_passant: None,
            };

            let mut y = 7;
            for rank in pieces {
                for (x, piece) in rank.into_iter().enumerate() {
                    let x = x as i8;
                    board.set([x, y], piece);
                }
                y -= 1;
            }

            board.to_move = match to_move {
                'w' => Color::White,
                'b' => Color::Black,
                _ => unreachable!(),
            };

            for castle in castling {
                match castle {
                    'K' => board.set_castles_kingside(Color::White, true),
                    'Q' => board.set_castles_queenside(Color::White, true),
                    'k' => board.set_castles_kingside(Color::Black, true),
                    'q' => board.set_castles_queenside(Color::Black, true),
                    _ => unreachable!(),
                }
            }

            match en_passant {
                Some((file, rank)) => {
                    let rank = (rank.to_digit(10).unwrap() - 1) as u8;
                    board.en_passant = Some(coord(file, rank).unwrap());
                }
                None => (),
            }

            Ok((i, board))
        }

        let (_, board) = parse_board(input).finish()?;

        Ok(board)
    }
}

#[derive(Clone, Debug, thiserror::Error)]
#[repr(C)]
pub enum Error {
    #[error("invalid piece bits")]
    InvalidPieceBits,

    #[error("no piece at coordinates")]
    NoPieceAtCoordinates,

    #[error("no en passant target")]
    NoEnPassantTarget,

    #[error("invalid move")]
    InvalidMove,

    #[error("invalid coordinates")]
    InvalidCoord,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn board_from_fen() {
        let _board = Board::from_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1").unwrap();
    }

    #[test]
    fn coord_works() -> Result<(), Error> {
        for file in "abcdefgh".chars() {
            for rank in 1..=8 {
                coord(file, rank)?;
            }
        }
        Ok(())
    }

    #[test]
    fn board_from_fen_correct() -> Result<(), Error> {
        use super::{PieceKind::*, Color::*};

        let fen_board = Board::from_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1").unwrap();

        let mut board = Board {
            squares: Default::default(),
            to_move: Color::White,
            castles: Default::default(),
            en_passant: None,
        };

        board.set_castles_kingside(Color::White, true);
        board.set_castles_queenside(Color::White, true);
        board.set_castles_kingside(Color::Black, true);
        board.set_castles_queenside(Color::Black, true);

        for (color, rank) in [(White, 1), (Black, 8)] {
            board.set(coord('a', rank)?, Some(Piece(color, Rook)));
            board.set(coord('b', rank)?, Some(Piece(color, Knight)));
            board.set(coord('c', rank)?, Some(Piece(color, Bishop)));
            board.set(coord('d', rank)?, Some(Piece(color, Queen)));
            board.set(coord('e', rank)?, Some(Piece(color, King)));
            board.set(coord('f', rank)?, Some(Piece(color, Bishop)));
            board.set(coord('g', rank)?, Some(Piece(color, Knight)));
            board.set(coord('h', rank)?, Some(Piece(color, Rook)));
        }

        for (color, rank) in [(White, 2), (Black, 7)] {
            for file in "abcdefgh".chars() {
                board.set(coord(file, rank)?, Some(Piece(color, Pawn)));
            }
        }

        assert!(&fen_board == &board);

        Ok(())
    }
}
